#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/gpio.h>
#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/workqueue.h>
#include <linux/fs.h>
#include <linux/cdev.h>

#define DECIMAL_DIGITS(x) ((8 * sizeof(x) * 28) / 93 + 2)

MODULE_LICENSE("Dual BSD/GPL");

static unsigned int gpio_num = 24;
static const char gpio_name[] = "GPIO24";
static int irq_num;

static void workqueue_task(struct work_struct *work);
static DECLARE_WORK(gpio_workqueue_task, workqueue_task);
static struct workqueue_struct *gpio_workqueue;

static spinlock_t irq_counter_lock;
static unsigned int irq_counter;
DECLARE_WAIT_QUEUE_HEAD(task_wait_queue);

static struct cdev gpio_cdev;
static dev_t dev;

static int gpio_open(struct inode *inode, struct file *filp)
{
	return 0;
}

static int gpio_release(struct inode *inode, struct file *filp)
{
	return 0;
}

static ssize_t gpio_read(struct file *filp, char __user *buf,
	size_t count, loff_t *f_pos)
{
	char buffer[DECIMAL_DIGITS(unsigned int) + 1];
	int n;

	n = sprintf(buffer, "%u", irq_counter);
	if (n < 0)
		return n;
	if (copy_to_user(buf, buffer, n))
		return -EFAULT;

	return n;
}

static ssize_t gpio_write(struct file *filp, const char __user *buf,
	size_t count, loff_t *f_pos)
{
	return count;
}

static const struct file_operations gpio_fops = {
	.read = gpio_read,
	.write = gpio_write,
	.open = gpio_open,
	.release = gpio_release,
	.owner = THIS_MODULE
};

static void workqueue_task(struct work_struct *work)
{
	unsigned long flags;

	spin_lock_irqsave(&irq_counter_lock, flags);

	while (irq_counter % 6 != 0) {
		spin_unlock_irqrestore(&irq_counter_lock, flags);
		if (wait_event_interruptible(task_wait_queue, irq_counter % 6 == 0))
			return; //got some signal
		spin_lock_irqsave(&irq_counter_lock, flags);
	}

	spin_unlock_irqrestore(&irq_counter_lock, flags);
}

static irqreturn_t irq_handler(int irq, void *dev_id)
{
	unsigned int mod;

	spin_lock(&irq_counter_lock);
	irq_counter++;
	mod = irq_counter % 6;
	spin_unlock(&irq_counter_lock);

	if (mod == 1)
		queue_work(gpio_workqueue, &gpio_workqueue_task);
	else if (mod == 0)
		wake_up_interruptible(&task_wait_queue);

	return IRQ_HANDLED;
}

static void gpio_irq_module_exit(void)
{
	if (irq_num > 0)
		free_irq(irq_num, NULL /*dev_id*/);
	gpio_free(gpio_num);

	cdev_del(&gpio_cdev);
	unregister_chrdev_region(dev, 1 /*count*/);

	if (gpio_workqueue) {
		flush_workqueue(gpio_workqueue);
		destroy_workqueue(gpio_workqueue);
	}
}

static int gpio_cdev_init(void)
{
	int retval;

	retval = alloc_chrdev_region(&dev, 0 /*firstminor*/, 1 /*count*/,
		"gpio_irq" /*name*/);
	if (retval < 0) {
		pr_info("gpio_irq: can't get major number: %d\n", retval);
		return retval;
	}

	cdev_init(&gpio_cdev, &gpio_fops);
	gpio_cdev.owner = THIS_MODULE;

	retval = cdev_add(&gpio_cdev, dev, 1 /*count*/);
	if (retval < 0) {
		pr_info("gpio_irq: error adding device: %d\n", retval);
		unregister_chrdev_region(dev, 1 /*count*/);
	}

	return retval;
}

static int gpio_init(void)
{
	int retval;

	retval = gpio_request(gpio_num, gpio_name);
	if (retval < 0) {
		pr_info("gpio_irq: can't get gpio: %d\n", retval);
		return retval;
	}
	retval = gpio_direction_input(gpio_num);
	if (retval < 0) {
		pr_info("gpio_irq: can't set gpio direction: %d\n", retval);
		return retval;
	}
	irq_num = gpio_to_irq(gpio_num);
	if (irq_num < 0) {
		pr_info("gpio_irq: can't map gpio number to irq number: %d\n",
			irq_num);
		return irq_num;
	}
	retval = request_irq(irq_num, irq_handler, 0 /*flags*/,
		"gpio_irq" /*dev_name*/, NULL /*dev_id*/);
	if (retval < 0) {
		pr_info("gpio_irq: can't set interrupt handler: %d\n", retval);
		return retval;
	}
	retval = irq_set_irq_type(irq_num, IRQ_TYPE_EDGE_RISING);
	if (retval < 0) {
		pr_info("gpio_irq: can't set irq type: %d\n", retval);
		return retval;
	}

	return retval;
}

static int gpio_irq_module_init(void)
{
	int retval;

	retval = gpio_cdev_init();
	if (retval < 0)
		return retval;

	retval = gpio_init();
	if (retval < 0)
		goto fail;

	spin_lock_init(&irq_counter_lock);

	gpio_workqueue = create_singlethread_workqueue("gpio_workqueue");
	if (!gpio_workqueue) {
		pr_info("gpio_irq: can't create workqueue\n");
		retval = -ENOMEM;
		goto fail;
	}

	return retval;

fail:
	gpio_irq_module_exit();
	return retval;
}

module_init(gpio_irq_module_init);
module_exit(gpio_irq_module_exit);
