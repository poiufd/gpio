#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>

#define BUFFERSIZE 4096

char buffer[BUFFERSIZE];

int main(int argc, char **argv)
{
    int n;
    int fd;

    fd = open("/dev/gpio_irq", O_RDONLY);
    if (fd < 0) {
        printf("error openning file\n");
        return 1;
    }

    while (1) {
        n = read(fd, buffer, BUFFERSIZE - 1);
        if (n >= 0) {
            buffer[n] = '\0';
            printf("irq_counter = %s\n", buffer);
        }
        if (n < 0)
            break;
        sleep(10);
    }

    return 1;
}
