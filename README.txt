SETUP:

1. compile
	dtc -@ -I dts -O dtb -o gpio7.dtbo gpio7.dts
2. put
	cp gpio7.dtbo /boot/overlays/
3. edit /boot/config.txt
	dtoverlay=gpio7
4. reboot

BEFORE:

pi@raspberrypi:~/src $ raspi-gpio get 7
GPIO 7: level=1 fsel=0 func=INPUT

AFTER:

pi@raspberrypi:~ $ sudo raspi-gpio get 7
GPIO 7: level=0 fsel=1 func=OUTPUT

pi@raspberrypi:~ $ dtc -I fs -O dts /sys/firmware/devicetree/base
...
gpio7 {
	brcm,pins = < 0x07 >;
	phandle = < 0x7a >;
	brcm,function = < 0x01 >;
};
...
